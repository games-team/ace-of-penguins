#!/usr/bin/make -f

-include /usr/share/dpkg/buildtools.mk
CC_FOR_BUILD ?= $(CC)

PACKAGE		= ace-of-penguins
PRGPREFIX	= ace-
ICON_NAME	= ace.png
ICON_SRC	= desktop/ace.png

# list of programs taken from games/Makefile
PROGS = \
 canfield \
 freecell \
 golf \
 mastermind \
 merlin \
 minesweeper \
 pegged \
 solitaire \
 spider \
 taipedit \
 taipei \
 thornq

CFLAGS  += -D_FILE_OFFSET_BITS=64
cc      := $(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS)

CONFFLAGS = --prefix=/usr --bindir=/usr/games

include debian/debian-vars.mk

export DEB_BUILD_MAINT_OPTIONS	= hardening=+all

# NOTE: The sources won't compile with -pedantic
export DEB_CFLAGS_MAINT_APPEND	= -Wall $(CPPFLAGS)

MANDIR = $(MANROOTDIR)/man6

#######################################################################

override_dh_auto_configure:
	CC="$(cc)" dh_auto_configure -- $(CONFFLAGS)

override_dh_auto_build: build-man
	dh_auto_build -- CC="$(cc)" BUILD_CC="$(CC_FOR_BUILD)"

test:
	# target: test

	@echo "# Display commands to test programs. Feed through '| sh'" >&2

	@for prog in $(PROGS); \
	do \
	    echo "$$prog &"; \
	done

man:
	# target: man
	$(MAKE) -C debian -f pod2man.mk PACKAGE=$(PACKAGE) \
	MANSECT=6 makeman

clean-man:
	# target: clean-man - remove manual page links (will be generated)

	for NAME in $(PROGS); \
	do \
		man=$(DEBDIR)/$(PRGPREFIX)$$NAME.6; \
		rm -f $$man; \
	done || :

build-man: man
	# target: build-man - link pages to master page
	for NAME in $(PROGS); \
	do \
	    man=debian/$(PRGPREFIX)$$NAME.6; \
	    echo ".so man6/ace-of-penguins.6" > $$man; \
	done

override_dh_auto_test:
	# nothing to test

override_dh_clean: clean-man
	#  target: clean - You may safely ignore message "invalid host type"
	if [ -f Makefile ]; then \
	    $(MAKE) \
		to_srcdir="$(TOPDIR)" \
		top_builddir="$(TOPDIR)" \
		clean distclean \
		|| : ; \
	fi

	rm -rf lib/.lib lib/.deps games/.lib games/.deps */*.[ao] *.[ao]

	dh_clean

override_dh_installman:
	dh_installman
	# Check that all is in order
	echo "PWD: pwd"
	( cd $(MANDIR) && ls -l )

override_dh_auto_install:
	dh_auto_install

	# rename programs to start with prefix
	for NAME in $(PROGS) ; do \
	    mv $(PKGDIR)/usr/games/$$NAME \
	       $(PKGDIR)/usr/games/$(PRGPREFIX)$$NAME || exit 1; \
	done

	$(INSTALL_DIR) $(PKGDIR)/usr/share/applications
	$(INSTALL_DATA) debian/desktop/*.desktop $(PKGDIR)/usr/share/applications

	# delete test program
	rm -f $(PKGDIR)/usr/games/penguins

	# Not used in Debian
	find $(PKGDIR) -name "*.la" | xargs --no-run-if-empty rm -f

	# Install custom icon
	install -D -m 644 $(ICON_SRC) $(ICONDIR)/hicolor/48x48/apps/$(ICON_NAME)

override_dh_installdocs:
	# Policy: COPYING file is not needed
	dh_installdocs -X COPYING -X .git -X CVS -X .svn docs/*

%:
	dh $@

.PHONY: test man build-man clean-man

# End of file
